from abc import ABC, abstractmethod
from itertools import product
from typing import List, Tuple

import numpy as np
import scipy.special

from preprocessing import TokenizedSentencePair


class BaseAligner(ABC):
    """
    Describes a public interface for word alignment models.
    """

    @abstractmethod
    def fit(self, parallel_corpus: List[TokenizedSentencePair]):
        """
        Estimate alignment model parameters from a collection of parallel sentences.

        Args:
            parallel_corpus: list of sentences with translations, given as numpy arrays of vocabulary indices

        Returns:
        """
        pass

    @abstractmethod
    def align(
        self, sentences: List[TokenizedSentencePair]
    ) -> List[List[Tuple[int, int]]]:
        """
        Given a list of tokenized sentences, predict alignments of source and target words.

        Args:
            sentences: list of sentences with translations, given as numpy arrays of vocabulary indices

        Returns:
            alignments: list of alignments for each sentence pair, i.e. lists of tuples (source_pos, target_pos).
            Alignment positions in sentences start from 1.
        """
        pass


class DiceAligner(BaseAligner):
    def __init__(self, num_source_words: int, num_target_words: int, threshold=0.5):
        self.cooc = np.zeros((num_source_words, num_target_words), dtype=np.uint32)
        self.dice_scores = None
        self.threshold = threshold

    def fit(self, parallel_corpus):
        for sentence in parallel_corpus:
            # use np.unique, because for a pair of words we add 1 only once for each sentence
            for source_token in np.unique(sentence.source_tokens):
                for target_token in np.unique(sentence.target_tokens):
                    self.cooc[source_token, target_token] += 1
        self.dice_scores = (
            2
            * self.cooc.astype(np.float32)
            / (self.cooc.sum(0, keepdims=True) + self.cooc.sum(1, keepdims=True))
        )

    def align(self, sentences):
        result = []
        for sentence in sentences:
            alignment = []
            for (i, source_token), (j, target_token) in product(
                enumerate(sentence.source_tokens, 1),
                enumerate(sentence.target_tokens, 1),
            ):
                if self.dice_scores[source_token, target_token] > self.threshold:
                    alignment.append((i, j))
            result.append(alignment)
        return result


class WordAligner(BaseAligner):
    def __init__(
        self, num_source_words, num_target_words, num_iters, use_log_proba=False
    ):
        self.num_source_words = num_source_words
        self.num_target_words = num_target_words
        self.translation_probs = np.full(
            (num_source_words, num_target_words), 1 / num_target_words, dtype=np.float32
        )
        self.num_iters = num_iters
        self.use_log_proba = use_log_proba
        if use_log_proba:
            self.translation_probs = np.log(
                self.translation_probs, out=self.translation_probs
            )

    def _e_step(self, parallel_corpus: List[TokenizedSentencePair]) -> List[np.array]:
        """
        Given a parallel corpus and current model parameters, get a posterior distribution over alignments for each
        sentence pair.

        Args:
            parallel_corpus: list of sentences with translations, given as numpy arrays of vocabulary indices

        Returns:
            posteriors: list of np.arrays with shape (src_len, target_len). posteriors[i][j][k] gives a posterior
            probability of target token k to be aligned to source token j in a sentence i.
        """
        posteriors = []

        for sentence in parallel_corpus:
            theta_rel = self.translation_probs[
                np.ix_(sentence.source_tokens, sentence.target_tokens)
            ]
            if self.use_log_proba:
                theta_rel -= scipy.special.logsumexp(theta_rel, axis=0, keepdims=True)
            else:
                theta_rel /= theta_rel.sum(axis=0, keepdims=True)
            posteriors.append(theta_rel)

        return posteriors

    def _compute_elbo(
        self, parallel_corpus: List[TokenizedSentencePair], posteriors: List[np.array]
    ) -> float:
        """
        Compute evidence (incomplete likelihood) lower bound for a model given data and the posterior distribution
        over latent variables.

        Args:
            parallel_corpus: list of sentences with translations, given as numpy arrays of vocabulary indices
            posteriors: posterior alignment probabilities for parallel sentence pairs (see WordAligner._e_step).

        Returns:
            elbo: the value of evidence lower bound
        """
        assert len(parallel_corpus) == len(posteriors)
        elbo = 0

        for sentence, posterior in zip(parallel_corpus, posteriors):
            theta_rel = self.translation_probs[
                np.ix_(sentence.source_tokens, sentence.target_tokens)
            ]
            if self.use_log_proba:
                theta_rel -= np.log(sentence.source_tokens.size)
                expt_post = np.exp(posterior)
                # in this case exp(-inf) * -inf = 0 * -inf = 0
                theta_rel[expt_post == 0] = 0
                theta_rel -= posterior  # already log from _e_step
                theta_rel[expt_post == 0] = 0
                theta_rel *= expt_post
            else:
                theta_rel *= 1 / sentence.source_tokens.size
                theta_rel /= posterior
                theta_rel = np.log(theta_rel, out=theta_rel)
                theta_rel *= posterior
            elbo += theta_rel.sum()

        return elbo

    def _m_step(
        self, parallel_corpus: List[TokenizedSentencePair], posteriors: List[np.array]
    ):
        """
        Update model parameters from a parallel corpus and posterior alignment distribution. Also, compute and return
        evidence lower bound after updating the parameters for logging purposes.

        Args:
            parallel_corpus: list of sentences with translations, given as numpy arrays of vocabulary indices
            posteriors: posterior alignment probabilities for parallel sentence pairs (see WordAligner._e_step).

        Returns:
            elbo:  the value of evidence lower bound after applying parameter updates
        """
        assert len(parallel_corpus) == len(posteriors)

        if self.use_log_proba:
            posterior_max = max(np.max(p) for p in posteriors)

        self.translation_probs[:] = 0
        for sentence, posterior in zip(parallel_corpus, posteriors):
            if self.use_log_proba:
                # partially "stolen" from scipy.special.logsumexp
                posterior = np.exp(posterior - posterior_max)
                np.add.at(
                    self.translation_probs,
                    np.ix_(sentence.source_tokens, sentence.target_tokens),
                    posterior,
                )
            else:
                np.add.at(
                    self.translation_probs,
                    np.ix_(sentence.source_tokens, sentence.target_tokens),
                    posterior,
                )

        if self.use_log_proba:
            with np.errstate(divide="ignore"):
                self.translation_probs = np.log(
                    self.translation_probs, out=self.translation_probs
                )
            self.translation_probs += posterior_max
            self.translation_probs -= scipy.special.logsumexp(
                self.translation_probs, axis=1, keepdims=True
            )
        else:
            self.translation_probs /= self.translation_probs.sum(axis=1, keepdims=True)

        return self._compute_elbo(parallel_corpus, posteriors)

    def fit(self, parallel_corpus):
        """
        Same as in the base class, but keep track of ELBO values to make sure that they are non-decreasing.
        Sorry for not sticking to my own interface ;)

        Args:
            parallel_corpus: list of sentences with translations, given as numpy arrays of vocabulary indices

        Returns:
            history: values of ELBO after each EM-step
        """
        history = []
        for i in range(self.num_iters):
            elbo = self._m_step(
                parallel_corpus,
                # Inline e-step to reduce posteriors ndarray lifetime
                posteriors=self._e_step(parallel_corpus),
            )
            history.append(elbo)
        return history

    def align(
        self, sentences: List[TokenizedSentencePair], threshold=1.0
    ) -> List[List[Tuple[int, int]]]:
        predictions = []
        for sentence in sentences:
            theta_rel = self.translation_probs[
                np.ix_(sentence.source_tokens, sentence.target_tokens)
            ]

            theta_idxs = theta_rel.argmax(axis=0)
            theta_prob = theta_rel[theta_idxs, range(theta_idxs.size)]
            if self.use_log_proba:
                theta_prob = np.exp(theta_prob)

            predictions.append(
                [
                    (1 + v, 1 + i)
                    for i, (v, p) in enumerate(zip(theta_idxs, theta_prob))
                    if p >= (1 - threshold)
                ]
            )
        return predictions


class WordPositionAligner(WordAligner):
    def __init__(self, num_source_words, num_target_words, num_iters):
        super().__init__(num_source_words, num_target_words, num_iters)
        self.alignment_probs = {}

    def _get_probs_for_lengths(self, src_length: int, tgt_length: int):
        """
        Given lengths of a source sentence and its translation, return the parameters of a "prior" distribution over
        alignment positions for these lengths. If these parameters are not initialized yet, first initialize
        them with a uniform distribution.

        Args:
            src_length: length of a source sentence
            tgt_length: length of a target sentence

        Returns:
            probs_for_lengths: np.array with shape (src_length, tgt_length)
        """
        pass

    def _e_step(self, parallel_corpus):
        pass

    def _compute_elbo(self, parallel_corpus, posteriors):
        pass

    def _m_step(self, parallel_corpus, posteriors):
        pass
