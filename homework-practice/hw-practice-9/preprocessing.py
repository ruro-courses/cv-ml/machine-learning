from collections import Counter
from dataclasses import dataclass
from typing import Dict, List, Tuple

import numpy as np
import xml.etree.ElementTree


@dataclass(frozen=True)
class SentencePair:
    """
    Contains lists of tokens (strings) for source and target sentence
    """

    source: List[str]
    target: List[str]


@dataclass(frozen=True)
class TokenizedSentencePair:
    """
    Contains arrays of token vocabulary indices (preferably np.int32) for source and target sentence
    """

    source_tokens: np.ndarray
    target_tokens: np.ndarray


@dataclass(frozen=True)
class LabeledAlignment:
    """
    Contains arrays of alignments (lists of tuples (source_pos, target_pos)) for a given sentence.
    Positions are numbered from 1.
    """

    sure: List[Tuple[int, int]]
    possible: List[Tuple[int, int]]


def extract_sentences(
    filename: str,
) -> Tuple[List[SentencePair], List[LabeledAlignment]]:
    """
    Given a file with tokenized parallel sentences and alignments in XML format, return a list of sentence pairs
    and alignments for each sentence.

    Args:
        filename: Name of the file containing XML markup for labeled alignments

    Returns:
        sentence_pairs: list of `SentencePair`s for each sentence in the file
        alignments: list of `LabeledAlignment`s corresponding to these sentences
    """
    with open(filename) as f:
        etree = xml.etree.ElementTree.fromstring(f.read().replace("&", "&amp;"))

    sentences = []
    alignments = []
    for s in etree:
        source = s.find("english").text or ""
        target = s.find("czech").text or ""
        sentences.append(SentencePair(source.split(), target.split()))

        sure = [
            tuple(int(v) for v in p.split("-"))
            for p in (s.find("sure").text or "").split()
        ]
        possible = [
            tuple(int(v) for v in p.split("-"))
            for p in (s.find("possible").text or "").split()
        ]
        alignments.append(LabeledAlignment(sure, possible))

    return sentences, alignments


def get_token_to_index(
    sentence_pairs: List[SentencePair], freq_cutoff=None
) -> Tuple[Dict[str, int], Dict[str, int]]:
    """
    Given a parallel corpus, create two dictionaries token->index for source and target language.

    Args:
        sentence_pairs: list of `SentencePair`s for token frequency estimation
        freq_cutoff: if not None, keep only freq_cutoff most frequent tokens in each language

    Returns:
        source_dict: mapping of token to a unique number (from 0 to vocabulary size) for source language
        target_dict: mapping of token to a unique number (from 0 to vocabulary size) target language

    """
    if freq_cutoff is not None:
        source_cnt = Counter()
        target_cnt = Counter()
    else:
        source_cnt = set()
        target_cnt = set()

    for pair in sentence_pairs:
        source_cnt.update(pair.source)
        target_cnt.update(pair.target)

    if freq_cutoff is not None:
        source_cnt = sorted(source_cnt, key=source_cnt.get, reverse=True)[:freq_cutoff]
        target_cnt = sorted(target_cnt, key=target_cnt.get, reverse=True)[:freq_cutoff]

    source_cnt = {k: i for i, k in enumerate(source_cnt)}
    target_cnt = {k: i for i, k in enumerate(target_cnt)}
    return source_cnt, target_cnt


def tokenize_sents(
    sentence_pairs: List[SentencePair], source_dict, target_dict
) -> List[TokenizedSentencePair]:
    """
    Given a parallel corpus and token_to_index for each language, transform each pair of sentences from lists
    of strings to arrays of integers. If either source or target sentence has no tokens that occur in corresponding
    token_to_index, do not include this pair in the result.

    Args:
        sentence_pairs: list of `SentencePair`s for transformation
        source_dict: mapping of token to a unique number for source language
        target_dict: mapping of token to a unique number for target language

    Returns:
        tokenized_sentence_pairs: sentences from sentence_pairs, tokenized using source_dict and target_dict
    """
    tokenized = []
    for pair in sentence_pairs:
        source_toks = []
        target_toks = []

        for s in pair.source:
            s = source_dict.get(s, None)
            if s is not None:
                source_toks.append(s)

        for t in pair.target:
            t = target_dict.get(t, None)
            if t is not None:
                target_toks.append(t)

        if source_toks and target_toks:
            tokenized.append(
                TokenizedSentencePair(
                    np.array(source_toks, dtype=np.int32),
                    np.array(target_toks, dtype=np.int32),
                )
            )
    return tokenized
