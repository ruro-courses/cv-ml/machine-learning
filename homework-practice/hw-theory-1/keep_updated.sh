#!/bin/sh
evince "task.pdf" >/dev/null 2>/dev/null &
find -type f | rg -v '(build|\.d)' | rg '\.(tex|bst|sty|bib|pdf|png|jpg)' | sort | \
    entr -as "./update_pdf.sh && evince task.pdf"
