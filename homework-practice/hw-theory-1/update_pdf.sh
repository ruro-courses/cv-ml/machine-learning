#!/bin/sh

mkdir -p build
texfot pdflatex \
    -interaction=nonstopmode \
    -halt-on-error \
    -synctex=1 \
    -file-line-error \
    -output-directory=build \
    -shell-escape \
    task.tex \
    && echo -ne "\n====>  Done!  <====\n" \
    || echo -ne "\n====> Failed! <====\n"
